<?php
function ubah_huruf($string){
    $abjad=['a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z'];
    $hasil="";
    for ($j=0 ; $j < strlen($string) ; $j++) {
        for ($i=0 ; $i < count($abjad) ; $i++)
        {
            if($abjad[$i]==$string[$j]){
            echo $abjad[$i]." => " .$abjad[$i+1]. "<br>";
             $hasil .=$abjad[$i+1];
            }
        } 
       }
       echo"$hasil <br><br>";
}


// TEST CASES
echo ubah_huruf('wow'); // xpx
echo ubah_huruf('developer'); // efwfmpqfs
echo ubah_huruf('laravel'); // mbsbwfm
echo ubah_huruf('keren'); // lfsfo
echo ubah_huruf('semangat'); // tfnbohbu

?>

<!-- buatlah sebuah file dengan nama ubah-huruf.php. Di dalam file 
tersebut buatlah sebuah function dengan nama ubah_huruf yang menerima
 parameter berupa string. function akan mereturn string yang berisi 
 karakter-karakter yang sudah diubah dengan karakter setelahnya dalam susunan
 abjad “abdcde …. xyz”. Contohnya karakter “a” akan diubah menjadi 
 “b” karakter “x” akan berubah menjadi “y”, dst. -->